# tictactoebot
## A robot that plays tic-tac-toe

Team members: Aleksander Parelo and Meelis Pihlap 

# PROOF OF CONCEPT

We have combined the game logic and image processing as a proof of concept.
Video can be seen here: 
https://drive.google.com/file/d/1ZKzN6KwqZX5DFqX9IRctbGdPeb067yTd/view?usp=sharing

## Challenges and solutions
The biggest challenge ahead of us is drawing symbols using gopigo as a platform. We intend to solve this using a linear actuator as described below. Should that not work out, however, we will use a bigger gameboard and more robust symbols in lieu of the standard X and O. 
We are confident that we will overcome this challenge.

# Project overview
The main body of the robot will be GoPiGo robot that will only move along a single axis. Attached to that will be a servo that moves linearly from side to side. Something similar to [this](https://www.youtube.com/watch?v=Sn-BklfEho0).

The robot will see the field using a fixed webcam directly above the field. The webcam will take a picture of the field and then use image recognition to convert the image to a field representation that the algorithm can use.

The robot will make moves based on a MinMax algorithm that will make the robot unbeatable, the algorithm will be based on the one described in [this article](https://www.neverstopbuilding.com/blog/2013/12/13/tic-tac-toe-understanding-the-minimax-algorithm13).

The field will be fixed in place and pre drawn, the robot will clear the field after every move so that the camera can get a clear picture of the field. The robot and a human player will take turns making moves. The beginner is chosen randomly and is indicated by the LEDs on the robot.

# Component list
component|how many|do we have it
---------|--------|-------------
Raspberry Pi (preferably 3)| 1 | NO
Webcam | 1 | NO
servo(s) | 2 | NO
GoPiGo | 1 | NO
pen| 1 | YES
other mechanical components | TBD | TBD

## Partial list of mechanical components we will probably need
Hopefully we can get some of these 3D printed at TÜTI. linear actuator for the servo, pen holder, camera mount

# Timeline
This is a preliminary timeline, the goals here are just guidelines. We will most likely work on different things concurrently.

* ## 16.04 - 22.04
	game logic
* ## 23.04 - 29.04
	* recognise field from image
	* convert to suitable format for game logic
* ## 30.04 - 06.05
	* some kind of hardware for POC
	* ### 03.05 - proof of concept
* ## 07.05 - 13.05
	* final hardware design
	* build hardware
* ## 14.05 - 20.05
	* debug hardware and software
	* ### 17.05 - project poster

## 21.05 - Demo

