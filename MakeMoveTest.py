import cv2
from MovementLogic import MakeMove
from TCP.Server import Server
from PCDriver import PCDriver
from DetectGameState import detectGameState, getMidPoint
import sys

try:
    PORT = int(sys.argv[1])
except:
    PORT = 1337
def connectToRobot():
    serv = Server(PORT)
    driver = PCDriver(serv)
    serv.startServer()
    serv.waitConnection()
    return driver, serv
def stopBot(serv, cap):
    serv.close()
    cap.release()
    cv2.destroyAllWindows()
    # When everything done, release the capture
    print 'Program shut down!'

# open the camera
CAMERA_ID = 1
cap = cv2.VideoCapture(CAMERA_ID)
driver, serv = connectToRobot()
destination_coords = (364, 183)

MakeMove(cap, driver, destination_coords)
driver.kill()
stopBot(serv, cap)
