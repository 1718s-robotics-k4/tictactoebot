import math
import numpy as np

def getVector(pt1, pt2):
    return (pt2[0]-pt1[0], pt2[1]-pt1[1])
def vectorMagnitude(vector):
    return (vector[0]**2 + vector[1]**2)**0.5
def dotProduct(v1, v2):
    return v1[0]*v2[0] + v1[1]*v2[1]
def getShortestAngle(v1, v2):
    dot = dotProduct(v1, v2)
    mag1 = vectorMagnitude(v1)
    mag2 = vectorMagnitude(v2)
    div = dot/(mag1*mag2)
    if div > 1 or div < -1:     # Python does weird division if angle is 0 resulting in an argument >1 for acos
        return 0
    try:
        return math.acos(div)*180/math.pi
    except Exception as e:
        print("div", div)
        raise e
def getDirection(v1, v2):
    # -1 = left
    # 1 = right
    # 0 = no movement
    cross = np.cross(np.array(v1), np.array(v2))
    if cross == 0.0:
        return 0
    elif cross < 0.0:
        return -1
    else:
        return 1