import cv2
import numpy as np
from DetectGameState import detectGameState, getMidPoint
from new_game_logic import getMove
from game_logic import getStatus, printBoard
from PCDriver import PCDriver
from TCP.Server import *
from MovementLogic import MakeMove
from random import randint
import sys

try:
    PORT = int(sys.argv[1])
except:
    PORT = 1337
field = [0]*9
grid = [[0]]*9
move = -1
updateField = True
def robotMakeMove(index):
    destination = getMidPoint(grid, index)
    MakeMove(video_capture_device, driver, destination, MOVEMENT_ENABLED=True)

def connectToRobot():
    serv = Server(PORT)
    driver = PCDriver(serv)
    serv.startServer()
    serv.waitConnection()
    return driver, serv
def stopBot(serv, cap):
    serv.close()
    cap.release()
    cv2.destroyAllWindows()
    # When everything done, release the capture
# open the camera
CAMERA_ID = 1
video_capture_device = cv2.VideoCapture(CAMERA_ID)
driver, serv = connectToRobot()

ROBOT_PLAYER = 1
HUMAN_PLAYER = 2
CURRENT_PLAYER = randint(1, 2)
#CURRENT_PLAYER = ROBOT_PLAYER
#gameOver = 0
gameStarted = False

print("Press P to start game.")
while True:
    Ret, frame = video_capture_device.read()
    field, grid = detectGameState(frame, updateField=updateField)
    if gameStarted:
        if CURRENT_PLAYER != HUMAN_PLAYER:
            printBoard(field)
            status = getStatus(field)
            if status != 0:
                #gameOver = 1
                if status == HUMAN_PLAYER:
                    print("Human cheated!")
                elif status == ROBOT_PLAYER:
                    print("Robot won!")
                    driver.victoryDance()
                else:
                    print("Draw, everyone loses.")
                break
            move = getMove(field, ROBOT_PLAYER)
            robotMakeMove(move)
            CURRENT_PLAYER = HUMAN_PLAYER
            printBoard(field)
            status = getStatus(field)
            if status != 0:
                #gameOver = 1
                if status == HUMAN_PLAYER:
                    print("Human cheated!!!!")
                elif status == ROBOT_PLAYER:
                    print("Robot won!")
                    driver.victoryDance()
                else:
                    print("Draw, everyone loses.")
                break
        else:
            cv2.putText(frame, "Press SPACE when done with turn.", 
                (10, 450), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 0, 0), 1)

    cv2.imshow("Result", frame)

    # Quit the program when 'q' is pressed
    keypress = cv2.waitKey(1)
    if keypress & 0xFF == ord('q'):
        break
    elif keypress & 0xFF == ord('p'):
        gameStarted = not gameStarted
    elif keypress & 0xFF == ord(' '):
        CURRENT_PLAYER = ROBOT_PLAYER
        #printBoard(field)
    elif keypress & 0xFF == ord('f'):
        updateField = not updateField
driver.kill()
stopBot(serv, video_capture_device)