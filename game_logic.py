from copy import deepcopy


def getMove(player, board):
    '''
    minmax function that returns the best move
    for the selected player

    uses a recursive helper function
    minmax_recursive()
    '''

    # set the opponent
    if player == 1:
        opponent = 2
    else:
        opponent = 1

    # create an empty array for scores of moves
    # needs to be initialized to -2 because scores can be between -10 and 10
    # and we need to pick the highest

    points = 9*[-20]

    # Iterate over the field
    for i, field in enumerate(board):
        if field == 0:  # if the spot is empty
            board[i] = player  # make a move
            # check the result
            status = getStatus(board)
            # if the game ends store the score
            if status == player:
                points[i] = 10
            elif status == opponent:
                points[i] = -10
            elif status == -1:
                points[i] = 0
            else:
                # call the recursive helper to get the score
                result = minmax_recursive(board, player, opponent, 1)
                points[i] = result
            # undo the move
            board[i] = 0
    # find which move had the highest score
    move = -1
    maxPoints = -2000
    for i, point in enumerate(points):
        if point > maxPoints:
            maxPoints = point
            move = i

    return move


def minmax_recursive(board, og_player, current_player, depth):
    '''
    Recursive helper function for getMove()
    Inputs
    board: the board
    og_player: the player originally specified in getMove()
    current_player: the player playing on this level
    depth: how many turns have been taken
    '''
    points = 9*[0]
    if current_player == 1:
        opponent = 2
    else:
        opponent = 1

    for i, field in enumerate(board):
        if field == 0:
            board[i] = current_player

            status = getStatus(board)
            if status == current_player:
                points[i] = depth - 10
            elif status == opponent:
                points[i] = 10 - depth
            elif status == -1:
                points[i] = 0
            else:
                result = minmax_recursive(board, og_player, opponent, depth+1)
                points[i] = result
            board[i] = 0
    if current_player == og_player:
        point = max(points)
    else:
        point = min(points)

    return point


def getStatus(board):
    '''
    return the current game status
    False for game still ongoing
    -1 for draw
    number of the winning player if somebody won
    '''
    winner = 0
    # check rows
    if board[0] == board[1] & board[1] == board[2]:
        winner = board[0]
    elif board[3] == board[4] & board[4] == board[5]:
        winner = board[3]
    elif board[6] == board[7] & board[7] == board[8]:
        winner = board[6]
    # check columns
    elif board[0] == board[3] & board[3] == board[6]:
        winner = board[0]
    elif board[1] == board[4] & board[4] == board[7]:
        winner = board[1]
    elif board[2] == board[5] & board[5] == board[8]:
        winner = board[2]
    # check diagonal
    elif board[0] == board[4] & board[4] == board[8]:
        winner = board[0]
    elif board[2] == board[4] & board[4] == board[6]:
        winner = board[2]
    else:
        if not (0 in board):
            return -1
        else:
            return False

    if winner != 0:
        return winner
    else:
        return False


def printBoard(board):
    '''
    prints the board
    :param list board: board as a list
    '''
    printBoard = deepcopy(board)
    for i, element in enumerate(printBoard):
        if element == 1:
            printBoard[i] = 'X'
        elif element == 2:
            printBoard[i] = 'O'
        else:
            printBoard[i] = ' '
    print("{0}|{1}|{2}".format(printBoard[0], printBoard[1], printBoard[2]))
    print("------")
    print("{0}|{1}|{2}".format(printBoard[3], printBoard[4], printBoard[5]))
    print("------")
    print("{0}|{1}|{2} \n".format(printBoard[6], printBoard[7], printBoard[8]))


def testGame():
    board = 9 * [0]

    while True:
        printBoard(board)
        move = int(input('move (1-9): '))
        board[move-1] = 1
        printBoard(board)
        if getStatus(board) != 0:
            break
        move = getMove(2, board)
        board[move] = 2
        printBoard(board)
        if(getStatus(board) != 0):
            break


#testGame()
