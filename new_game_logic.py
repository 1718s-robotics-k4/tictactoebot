from copy import deepcopy

EMPTY = 0
HUMAN_PLAYER = 1
AI_PLAYER = 2

class Move:
    def __init__(self, index, score):
        self.index = index
        self.score = score


def empty_spots(board):
    return [i for i in range(len(board)) if board[i] == EMPTY]
def getStatus(board):
    '''
    return the current game status
    False for game still ongoing
    -1 for draw
    number of the winning player if somebody won
    '''
    winner = 0
    # check rows
    if board[0] == board[1] & board[1] == board[2]:
        winner = board[0]
    elif board[3] == board[4] & board[4] == board[5]:
        winner = board[3]
    elif board[6] == board[7] & board[7] == board[8]:
        winner = board[6]
    # check columns
    elif board[0] == board[3] & board[3] == board[6]:
        winner = board[0]
    elif board[1] == board[4] & board[4] == board[7]:
        winner = board[1]
    elif board[2] == board[5] & board[5] == board[8]:
        winner = board[2]
    # check diagonal
    elif board[0] == board[4] & board[4] == board[8]:
        winner = board[0]
    elif board[2] == board[4] & board[4] == board[6]:
        winner = board[2]
    return winner
def getMove(board, player):
    return minimax(board, player).index

def minimax(board, player):
    playable_spots = empty_spots(board)
    status = getStatus(board)
    if status == AI_PLAYER:
        return Move(-1, 10)
    elif status == HUMAN_PLAYER:
        return Move(-1, -10)
    elif len(playable_spots) == 0:
        return Move(-1, 0)
    
    moves = []
    for i in range(len(playable_spots)):
        move = Move(playable_spots[i], -1)
        board[playable_spots[i]] = player
        if player == AI_PLAYER:
            result = minimax(board, HUMAN_PLAYER)
            move.score = result.score
        else:
            result = minimax(board, AI_PLAYER)
            move.score = result.score
        board[playable_spots[i]] = 0
        moves.append(move)
    if player == AI_PLAYER:
        bestScore = -10000
        for j, move in enumerate(moves):
            if move.score > bestScore:
                bestScore = move.score
                bestMove = j
    else:   
        bestScore = 10000
        for j, move in enumerate(moves):
            if move.score < bestScore:
                bestScore = move.score
                bestMove = j
    return moves[bestMove]

def printBoard(board):
    '''
    prints the board
    :param list board: board as a list
    '''
    printBoard = deepcopy(board)
    for i, element in enumerate(printBoard):
        if element == 1:
            printBoard[i] = 'X'
        elif element == 2:
            printBoard[i] = 'O'
        else:
            printBoard[i] = ' '
    print("{0}|{1}|{2}".format(printBoard[0], printBoard[1], printBoard[2]))
    print("------")
    print("{0}|{1}|{2}".format(printBoard[3], printBoard[4], printBoard[5]))
    print("------")
    print("{0}|{1}|{2} \n".format(printBoard[6], printBoard[7], printBoard[8]))

def testGame():
    board = 9 * [0]

    while True:
        printBoard(board)
        move = int(input('move (1-9): '))
        board[move-1] = 1
        printBoard(board)
        if getStatus(board) != 0:
            break
        move = getMove(board, 2)
        board[move] = 2
        printBoard(board)
        if(getStatus(board) != 0):
            break
#testGame()