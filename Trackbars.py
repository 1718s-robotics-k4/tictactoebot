# Separate file for trackbar functions to allow for cleaner main code.
import cv2

def setMaxCircle(new):
    global MAX_CIRCLE_AREA
    MAX_CIRCLE_AREA = new

def setMinGrid(new):
    global MIN_X_AREA
    MIN_X_AREA = new

def setMinCircle(new):
    global MIN_CIRCLE_AREA
    MIN_CIRCLE_AREA = new

def makeAreaTrackbars():
    cv2.namedWindow("Trackbars")
    cv2.createTrackbar("MAX_CIRCLE_AREA", "Trackbars", MAX_CIRCLE_AREA, 25000,setMaxCircle)
    cv2.createTrackbar("MIN_CIRCLE_AREA", "Trackbars", MIN_CIRCLE_AREA, 15000, setMaxCircle)
    cv2.createTrackbar("MIN_X_AREA", "Trackbars", MIN_X_AREA, 15000, setMinGrid)