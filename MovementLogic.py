#import gopigo as go
import time
import cv2
import numpy as np
import os.path
import math
from Vectors import *


TURN_TO_POINT = 0
DRIVE_TO_POINT = 1
DRAW_LINE = 2
TURN_BACK = 3
DRIVE_BACK = 4
TURN_TO_FIELD = 5

SPEED = 20
MID_ANGLE = 2                   # limit of vector angle offset
DISTANCE_OFFSET = 20
BWD_OFFSET = 20
MARKER_DISTANCE = 20
HOME = (590, 183)

FRONT_DOT_THRESH_FILE = "purpledot.txt"
BACK_DOT_THRESH_FILE = "yellowdot.txt"

def readThresh(THRESH_FILE):
    if os.path.isfile(THRESH_FILE):
        f = open(THRESH_FILE)
        lower = [int(i) for i in f.readline().split(",")]
        upper = [int(i) for i in f.readline().split(",")]
        f.close()
    else:
        lower = [122, 83, 202]
        upper = [242, 121, 242]
    return lower, upper

def getContourCenter(cont):
    M = cv2.moments(cont)       # Get contour moments
    cx = int(M['m10']/M['m00']) # Find centroid
    cy = int(M['m01']/M['m00'])
    return (cx, cy)

frontlower, frontupper = readThresh(FRONT_DOT_THRESH_FILE)
backlower, backupper = readThresh(BACK_DOT_THRESH_FILE)


def getSortedContours(frame, lowerLimits, upperLimits):
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    (cnts, _) = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(frame, cnts, -1, (255, 0, 0, 2))
    cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, (5,5), iterations = 3)
    return sorted(cnts, key=cv2.contourArea, reverse=True), outimage


def drawContourVectors(frame, frontcnts, backcnts, endpoint, printDiagnostics=False):
    if len(frontcnts) > 0 and cv2.contourArea(frontcnts[0]) > 0:
        if len(backcnts) > 0 and cv2.contourArea(backcnts[0]) > 0:
            fwdcont = frontcnts[0]
            backcont = backcnts[0]
            backcenter = getContourCenter(backcont)
            frontcenter = getContourCenter(fwdcont)
            vfront = getVector(backcenter, frontcenter)
            vend = getVector(backcenter, endpoint)
            vhome = getVector(backcenter, HOME)
            cv2.line(frame, frontcenter, backcenter,(0, 255, 0), 3)
            cv2.line(frame, backcenter, endpoint, (0, 255, 0), 3)
            cv2.line(frame, backcenter, HOME, (0, 255, 0), 3)
            if printDiagnostics:
                cv2.putText(frame, str(vectorMagnitude(vfront)), (20, 20), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2)
                cv2.putText(frame, str(vectorMagnitude(vend)), (20, 40), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2)
                cv2.putText(frame, str(vectorMagnitude(vhome)), (20, 60), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2)
                cv2.putText(frame, str(backcenter), backcenter, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2)
                cv2.putText(frame, str(frontcenter), frontcenter, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 2)
            return vfront, vend, vhome
        else:
            return -1, -1, -1
    else:
        return -1, -1, -1
def drawMarker(destination_coords, v1, v2, v3, driver, STATE):
    angle = getShortestAngle(v1, v2)
    if STATE == TURN_TO_POINT:
        #if turnToPoint(angle, driver, v1, v2):
        driver.turnDeg(getDirection(v1, v2), int(angle))
        STATE = DRIVE_TO_POINT
        print("Drive to point")
        return STATE
    elif STATE == DRIVE_TO_POINT:
        if driveToPoint(angle, driver, v1, v2):
            STATE = DRAW_LINE
            print("Draw line")
            return STATE
    elif STATE == DRAW_LINE:
        #if turnToPoint(angle, driver, v1, v2, speed=15):
        angle = getShortestAngle(v1, v2)
        if angle > MID_ANGLE:
            print("turndeg")
            driver.turnDeg(getDirection(v1, v2), int(angle))
        driver.drawLine(50)
        STATE = TURN_BACK
        print("Turning back")
        return STATE
    elif STATE == TURN_BACK:
        angle = getShortestAngle(v1, v3)
        #if turnToPoint(angle, driver, v1, v3, bwd=True):
        driver.turnDeg(-getDirection(v1, v3), 180-angle)
        STATE = DRIVE_BACK
        print("Driving back")
        return STATE
    elif STATE == DRIVE_BACK:
        angle = getShortestAngle(v1, v3)
        if driveToPoint(angle, driver, v1, v3, bw=True):
            STATE = TURN_TO_FIELD
            print("Turn towards field")
            return STATE
    elif STATE == TURN_TO_FIELD:
        #if turnToPoint(angle, driver, v1, v2):
        angle = getShortestAngle(v1, v2)
        driver.turnDeg(getDirection(v1, v2), angle)
        return -1
    return STATE

def turnToPoint(angle, driver, v1, v2, error=MID_ANGLE, speed=15, bwd=False):
    if not bwd:
        if angle > error:
            driver.turn_rot(getDirection(v1, v2), speed)
        else:
            return 1
    else:
        if angle < 180-(error + 10):
            driver.turn_rot(-getDirection(v1, v2), speed)
        else:
            return 1
    return 0

def driveToPoint(angle, driver, v1, v2, bw=False):
    if not bw:
        if not abs(vectorMagnitude(v2) - vectorMagnitude(v1)) > DISTANCE_OFFSET + MARKER_DISTANCE:
            driver.stop()
            return 1
    else:
        if not vectorMagnitude(v2) > DISTANCE_OFFSET + BWD_OFFSET:
            driver.stop()
            return 1
    if angle > MID_ANGLE:
        if not bw:
            driver.smoothTurn(25, 5, getDirection(v1, v2))
        else:
            driver.smoothTurn(-25, 5, getDirection(v1, v2))
    else:
        if not bw:
            driver.move(30)
        else:
            driver.move(-30)
    return 0
        
def MakeMove(cap, driver, destination_coords, MOVEMENT_ENABLED=False):
    MOVEMENT_ENABLED = MOVEMENT_ENABLED
    STATE = TURN_TO_POINT
    printDiagnostics = False
    while True:
        #read the image from the camera
        ret, frame = cap.read()

        #You will need this NOW
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        frontLowerLimits = np.array(frontlower)
        frontUpperLimits = np.array(frontupper)
        backLowerLimits = np.array(backlower)
        backUpperLimits = np.array(backupper)


        # Our operations on the frame come here
        frontcnts, frontoutimage = getSortedContours(frame, frontLowerLimits, frontUpperLimits)
        backcnts, backoutimage = getSortedContours(frame, backLowerLimits, backUpperLimits)

        outimage = cv2.bitwise_or(frontoutimage, backoutimage)
        cv2.circle(frame, destination_coords, 5, (0, 0, 255), 2)
        cv2.circle(frame, HOME, 5, (0, 0, 255), 2)
        
        v1, v2, v3 = drawContourVectors(frame, frontcnts, backcnts, destination_coords, printDiagnostics)
        if v1 != -1:
            cv2.putText(frame, str(getDirection(v1, v2)) , (10, 400), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 0, 0))
            if MOVEMENT_ENABLED:
                STATE = drawMarker(destination_coords, v1, v2, v3, driver, STATE)
                if STATE == -1:
                    driver.stop()
                    return
        else:
            if MOVEMENT_ENABLED:
                driver.move(SPEED)
        # Display the resulting frame
        cv2.imshow('processed', outimage)
        cv2.imshow("Result", frame)
        # Quit the program when Q is pressed
        key = cv2.waitKey(1)
        if key & 0xFF == ord('q'):
            driver.stop()
            break
        elif key & 0xFF == ord('p'):
            MOVEMENT_ENABLED = not MOVEMENT_ENABLED
            STATE = 0
        elif key & 0xFF == ord('.'):
            driver.penUp()
        elif key & 0xFF == ord('-'):
            driver.penDown()
        elif key & 0xFF == ord(','):
            printDiagnostics = not printDiagnostics
        if not MOVEMENT_ENABLED:
            driver.stop()
