import socket
from Driver import *
import sys

sock = socket.socket()
HOST = sys.argv[1]
PORT = int(sys.argv[2])

sock.connect((HOST, PORT))

functions = {'move':move, 'turn':turn, 'turnRot':turn_rot,
                 'moveDist':moveDistance, 'turnDeg':turnDeg, 'drawLine':drawLine,
                 'penUp':penUp, 'penDown':penDown, 'stop':stop, "smoothTurn":smoothTurn, 
                 "victoryDance":victoryDance}

def parseCommand(data):
    commands = data.split(":")
    return commands
def call_function(data):
    pieces = parseCommand(data)
    name = pieces[0]
    if len(pieces) > 1:
        params = pieces[1:]
        for i in range(len(params)):
            params[i] = int(params[i])
        functions.get(name)(*params) # I love Python
	print("Executed: " + name)
        if name == "drawLine":       # Handshake, send confirmation that line has been drawn.
            sock.sendall("done")
    else:
        functions.get(name)()
while True:
    data = sock.recv(1024)
    print("Received: ", repr(data)) 
    pieces = data.split(";")
    if pieces[0] == "kill":
        break
    for i in pieces[:-1]: # Last piece is always empty after ;
        call_function(i)    

sock.close()
