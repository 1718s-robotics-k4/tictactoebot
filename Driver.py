import gopigo as go
import serial
import time

port = '/dev/ttyUSB0'

ser = serial.Serial(port, 9600)

LEFT_SPEED_MODIFIER = 0
RIGHT_SPEED_MODIFIER = 0


def move(speed):
    go.set_speed(abs(speed))
    if speed > 0:
        go.fwd()
    else:
        go.bwd()

def turn_rot(dir, speed):
    go.set_speed(speed)
    if dir == 1:
        go.right_rot()
    else:
        go.left_rot()



def turn(dir, speed):
    go.set_speed(speed)
    if dir == 1:
        go.right()
    else:
        go.left()

def smoothTurn(speed, offset, dir):
    if dir == 1: # right
        go.set_right_speed(abs(speed))
        go.set_left_speed(abs(speed) + offset)
        if speed > 0:
            go.fwd()
        else:
            go.bwd()
    else:
        go.set_right_speed(abs(speed) + offset)
        go.set_left_speed(abs(speed))
        if speed > 0:
            go.fwd()
        else:
            go.bwd()


def moveDistance(speed, dist):
    # if distance is negative, go backwards
    # assuming each encoder tick is 10 mm
    # distance in mm
    # steps of 10 mm
    ticks = abs(dist // 10)
    go.enc_tgt(1, 1, ticks)
    go.set_left_speed(speed + LEFT_SPEED_MODIFIER)
    go.set_right_speed(speed + RIGHT_SPEED_MODIFIER)
    if dist > 0:
        go.fwd()
    else:
        go.bwd()
    while go.read_enc_status(): # Wait for encoders to finish
        time.sleep(0.001)

def turnDeg(dir, degrees, divider=27, speed=20):
    go.set_speed(speed)
    if dir == -1:
        go.left_rot()
    else:
        go.right_rot()
    time.sleep(float(degrees)/divider)
    go.stop()
def stop():
    go.stop()


def penUp():
    ser.write(b'1')
    print(ser.readline())


def penDown():
    ser.write(b'2')
    print(ser.readline())


def drawLine(length):
    penDown()
    moveDistance(50, length)
    penUp()
    moveDistance(50, -length/2)
    penDown()
    turnDeg(1, 10)
    turnDeg(-1, 20)
    penUp()
    turnDeg(1, 10)
    moveDistance(50, -length/2)

def victoryDance():
    turnDeg(1, 360, speed=40)
    turnDeg(-1, 360, speed=40)

