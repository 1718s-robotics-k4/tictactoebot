import cv2
from DetectGameState import detectGameState, showWindow, getMidPoint
from game_logic import printBoard
from MovementLogic import MakeMove
from TCP.Server import Server
from PCDriver import PCDriver
import sys

try:
    PORT = int(sys.argv[1])
except:
    PORT = 1337
def connectToRobot():
    serv = Server(PORT)
    driver = PCDriver(serv)
    serv.startServer()
    serv.waitConnection()
    return driver, serv
def stopBot(serv, cap):
    serv.close()
    cap.release()
    cv2.destroyAllWindows()
    # When everything done, release the capture
    print 'Program shut down!'

# open the camera
CAMERA_ID = 1
cap = cv2.VideoCapture(CAMERA_ID)
driver, serv = connectToRobot()
destination_coords = (426, 246)

#MakeMove(cap, driver, destination_coords)

updateField = True
while True:
    ret, frame = cap.read()
    field, grid = detectGameState(frame, updateField=updateField)
    showWindow("Result", frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('p'):
        printBoard(field)
        for i, val in enumerate(grid):
            print(val, getMidPoint(grid, i))
            MakeMove(cap, driver, getMidPoint(grid, i))
    elif key & 0xFF == ord('f'):
        updateField = not updateField

driver.kill()
stopBot(serv, cap)