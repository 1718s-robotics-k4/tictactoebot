import cv2
import sys
import numpy as np

class Trackbar:
    def cb(self, new_value):
        self.values[self.index] = new_value
    def __init__(self, windowName, label, values, index, 
            defaultValue=0, maxValue=255):
        self.values = values
        self.index = index
        cv2.createTrackbar(label, windowName, defaultValue, maxValue, self.cb)
lower = [0, 0, 0]
upper = [255, 255, 255]
try:
    THRESH_FILE = sys.argv[1]
except:
    THRESH_FILE = "frontdot.txt"
def write_thresh(lower, upper, filename=THRESH_FILE):
    f = open(filename, "w")
    f.write(str(lower[0]) + "," + str(lower[1]) + "," + str(lower[2]))
    f.write("\n")
    f.write(str(upper[0]) + "," + str(upper[1]) + "," + str(upper[2]))

sliderWindow = cv2.namedWindow("Sliders")

Trackbar("Sliders", "Hl", lower, 0, lower[0])
Trackbar("Sliders", "Sl", lower, 1, lower[1])
Trackbar("Sliders", "Vl", lower, 2, lower[2])

Trackbar("Sliders", "Hh", upper, 0, upper[0])
Trackbar("Sliders", "Sh", upper, 1, upper[1])
Trackbar("Sliders", "Vh", upper, 2, upper[2])

cap = cv2.VideoCapture(1)

while True:
    #read the image from the camera
    ret, frame = cap.read()
    
    #You will need this NOW
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    lowerLimits = np.array(lower)
    upperLimits = np.array(upper)
    
    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    
    # Display the resulting frame
    cv2.imshow('processed', outimage)
    cv2.imshow("original", frame)
    # Quit the program when Q is pressed
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('.'):
        print "saved new thresholds to " + THRESH_FILE
        write_thresh(lower, upper)
