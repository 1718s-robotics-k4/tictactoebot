import socket
import time

class Server:
    def __init__(self, port):
        self.sock = None
        self.port = port
        self.clientsocket = None
    def startServer(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(("", self.port))
        self.sock.listen(3)
        print("Hosting on IP: ", socket.gethostbyname(socket.gethostname()), self.port)
    def waitConnection(self):
        conn, addr = self.sock.accept()
        self.clientsocket = conn
        print("Connected by: ", addr)
        return conn
    def sendCommand(self, command):
        try:
            self.clientsocket.sendall(command+";") # Appending ; to avoid errors with too frequent sending
            time.sleep(0.01)
        except Exception as e:
            print(str(e))
            print("Closing server socket!")
            self.sock.close()
    def getResponse(self):
        print(self.clientsocket.recv(1024))
    def close(self):
        self.sock.close()
