# Detect game state using OpenCV
# We differentiate between circles/x-symbols and the grid using contour areas
# Things we rely on:
# The middle square is the second biggest contour by area
# The grid itself is the biggest contour

# TODO: Make a class to hold all gamefield related information 

import cv2
import numpy as np


MAX_CIRCLE_AREA = 10000 # Anything higher is the grid
MIN_X_AREA = 100       # Anything lower is noise
MAX_X_SOLIDITY = 0.6   # Anything more solid is a circle

GRID_THICKNESS = 10
MID_SQUARE_AREA = 0

gameField = [0]*9     # Gamestate
field = [[0]]*9       # List to keep locations of squares as rectangles on screen [x, y, w, h]

# Eventually write logic to set these automatically
def getMidPoint(field, field_index):
    x = field[field_index][0] + field[field_index][2]//2
    y = field[field_index][1] + field[field_index][3]//2
    return (x,y)

def sortContours(conts):
    # Analyzes contours and separates them by size and solidity
    x_conts = []
    o_conts = []
    grid_conts = []
    for cont in conts:
        area = cv2.contourArea(cont)
        if not (area < MIN_X_AREA or area >= MID_SQUARE_AREA - MID_SQUARE_AREA/10):
            solidity = float(area)/cv2.contourArea(cv2.convexHull(cont))
            if solidity < MAX_X_SOLIDITY:
                x_conts.append(cont)
            else:
                o_conts.append(cont)
        else:
            grid_conts.append(cont)
    return x_conts, o_conts, grid_conts

def showWindow(name, frame):    # Convenience function to create and show a window
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, 600, 600)
    cv2.imshow(name, frame)
def detectFields(middleSquare, field):  # Detect squares of gamefield
    x, y, w, h = cv2.boundingRect(middleSquare)
    gt = GRID_THICKNESS
    field[0] = [x - gt - w, y - gt - h, w, h]
    field[1] = [x, y - gt - h, w, h]
    field[2] = [x + gt + w, y - gt - h, w, h]
    field[3] = [x - gt - w, y, w, h]
    field[4] = [x, y, w, h]
    field[5] = [x + gt + w, y, w, h]
    field[6] = [x - gt - w, y + gt + h, w, h]
    field[7] = [x, y + gt + h, w, h]
    field[8] = [x + gt + w, y + gt + h, w, h]
    return field
def getSquare(field, cont):     # Find which square a contour belongs to
    M = cv2.moments(cont)       # Get contour moments
    cx = int(M['m10']/M['m00']) # Find centroid
    cy = int(M['m01']/M['m00'])

    for sq in field:
        if cx >= sq[0] and cx < sq[0] + sq[2]:
            if cy >= sq[1] and cy < sq[1] + sq[3]:
                return field.index(sq)
    
    # If contour not in grid, return -1
    return -1
    
def saveImage(frame, name="SavedBoard.PNG"):
    cv2.imwrite(name, frame)
def detectGameState(frame, updateField=True, showSeparate=False, getStart=False):
    global field
    global MID_SQUARE_AREA
    # Process image for better detection
    imgrey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # Turn to greyscale
    blur = cv2.GaussianBlur(imgrey, (5,5), 0)        # Apply blur
    thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 2)
    #thresh = cv2.erode(thresh, (7,7), iterations=1)
    #thresh = cv2.dilate(thresh, (13,13),iterations=3)
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, (7,7), iterations=5)
    showWindow("Thresh", thresh)
    #thresh = cv2.medianBlur(thresh, 7)
    #thresh = cv2.dilate(thresh, (47, 47),iterations=10)

    (cnts, _) = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # Find all contours
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)  # Sort contours by area
    if showSeparate:
        showContoursSeparate(cnts, frame)
    elif len(cnts) > 1:
        if len(field[0]) == 0 or updateField: # Only bother detecting field on inital run (it will get rekt)
            middleSquare = cnts[1]            # Second biggest contour is the middle square
            MID_SQUARE_AREA = cv2.contourArea(middleSquare)
            field = detectFields(middleSquare, field)   # Assign locations to squares based on middle square
        for sq in field:    # Draw rectangles
            cv2.rectangle(frame, (sq[0], sq[1]), (sq[0]+ sq[2], sq[1] + sq[3]), (0, 0, 255), 2)
        getMarkings(cnts, field, frame) # Detect markings on field
    return gameField, field


def getMarkings(cnts, field, frame, keepPrevData=False): # Sort contours and write symbols to board
    global gameField
    if not keepPrevData:
        gameField = [0]*9
    x_conts, o_conts, grid_cont = sortContours(cnts)
    #cv2.drawContours(frame, grid_cont, -1, (255, 0, 0), 2)
    real_x_conts = []
    real_o_conts = []
    for c in x_conts:
        index = getSquare(field, c)
        if index != -1:
            gameField[index] = 1 # x=1
            real_x_conts.append(c)
    for c in o_conts:
        index = getSquare(field, c)
        if index != -1:
            gameField[index] = 2 # O = 2
            real_o_conts.append(c)
    cv2.drawContours(frame, real_x_conts, -1, (0, 255, 0), 2)
    cv2.drawContours(frame, real_o_conts, -1, (0, 255, 255), 2)

def showContoursSeparate(cnts, frame):
    for i in range(len(cnts)):
        cnt = cnts[i]
        area = cv2.contourArea(cnt)
        hull = cv2.convexHull(cnt)
        hull_area = cv2.contourArea(hull)
        solidity = float(area)/hull_area
        print("Solidity: ", solidity)
        temp = frame[:]
        cv2.drawContours(temp, [cnts[i]], -1, (255, 0, 0), 2)
        print(cv2.contourArea(cnts[i]))
        showWindow("Contours", temp)
        cv2.waitKey(0)
        cv2.destroyWindow("Contours")

