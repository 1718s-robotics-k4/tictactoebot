import cv2
from DetectGameState import detectGameState, showWindow, getMidPoint
from game_logic import printBoard

# open the camera
CAMERA_ID = 1
cap = cv2.VideoCapture(CAMERA_ID)
updateField = True
while True:
    ret, frame = cap.read()
    field, grid = detectGameState(frame, updateField=updateField)
    showWindow("Result", frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('p'):
        printBoard(field)
        for i, val in enumerate(grid):
            print(val, getMidPoint(grid, i))
    elif key & 0xFF == ord('f'):
        updateField = not updateField
cap.release()
cv2.destroyAllWindows()