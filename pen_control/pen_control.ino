#include <Servo.h>

Servo myservo;

void penUp() {
  myservo.write(180);
  delay(200);
  myservo.write(92);
  Serial.println(1);
}

void penDown() {
  myservo.write(0);
  delay(200);
  myservo.write(92);
  Serial.println(1);
}

int dir = 0;

void setup() {
  myservo.attach(3);
  Serial.begin(9600);

}

void loop() {
  if(Serial.available()) {
    dir = Serial.parseInt();
    if(dir == 1) {
      penDown();
    }
    if(dir == 2) {
      penUp();
    }
  }
  else {
    myservo.write(92);
    delay(10);
  }

}
