class PCDriver:
    def __init__(self, server):
        self.server = server
        self.lastCommand = None
    def move(self, speed):
        if self.lastCommand != "move:"+str(speed):
            self.server.sendCommand("move:"+str(speed))
            self.lastCommand = "move:"+str(speed)
    def turn_rot(self, dir, speed):
        if self.lastCommand != "turnRot:"+str(dir)+":"+str(speed):
            self.server.sendCommand("turnRot:"+str(dir)+":"+str(speed))
            self.lastCommand = "turnRot:"+str(dir)+":"+str(speed)
    def turn(self, dir, speed):
        if self.lastCommand != "turn:"+str(dir)+":"+str(speed):
            self.server.sendCommand("turn:"+str(dir)+":"+str(speed))
            self.lastCommand = "turn:"+str(dir)+":"+str(speed)
    def moveDistance(self, speed, dist):
        if self.lastCommand != "moveDist:"+str(speed)+":"+str(dist):
            self.server.sendCommand("moveDist:"+str(speed)+":"+str(dist))
            self.lastCommand = "moveDist:"+str(speed)+":"+str(dist)
    def turnDeg(self, dir, degrees):
        degrees = int(degrees)
        if self.lastCommand != "turnDeg"+str(dir)+":"+str(degrees):
            self.server.sendCommand("turnDeg:"+str(dir)+":"+str(degrees))
            self.lastCommand = "turnDeg"+str(dir)+":"+str(degrees)
    def penUp(self):
        if self.lastCommand != "penUp":
            self.server.sendCommand("penUp")
            self.lastCommand = "penUp"
    def penDown(self):
        if self.lastCommand != "penDown":
            self.server.sendCommand("penDown")
            self.lastCommand = "penDown"
    def drawLine(self, length):
        if self.lastCommand != "drawLine:"+str(length):
            self.server.sendCommand("drawLine:"+str(length))
            self.lastCommand = "drawLine:"+str(length)
            self.server.getResponse() # NB! We block until line is drawn
    def stop(self):
        if self.lastCommand != "stop":
            self.server.sendCommand("stop")
            self.lastCommand = "stop"
    def smoothTurn(self, speed, offset, dir):
        if self.lastCommand != "smoothTurn:"+str(speed)+":"+str(offset)+":"+str(dir):
            self.server.sendCommand("smoothTurn:"+str(speed)+":"+str(offset)+":"+str(dir))
            self.lastCommand = "smoothTurn:"+str(speed)+":"+str(offset)+":"+str(dir)
    def kill(self):
        self.server.sendCommand("kill")
    def victoryDance(self):
        if self.lastCommand != "victoryDance":
            self.server.sendCommand("victoryDance")
            self.lastCommand = "victoryDance"